# weatheralerts

Short one line bash script to get the weather alerts for a given location. It is designed to be an add on script for the Chameleon conky script.

To change the location change the location INZ004 to your National Weather Service Zone.

Script requires curl and jq to run.

This script does not provide uptodate weather information.  
